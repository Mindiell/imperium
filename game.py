#encoding: utf-8
"""
Main game module.
"""

class Unit():
    def __init__(self, hits=0, movements=0, land=0, damages=1):
        self.max_hits = hits        # Number of hit this unit can take before destruction
        self.hits = 0               # Number of hit this unit has taken
        self.movements = movements  # Number of movements per turn
        self.moves = 0              # Number of moves done this turn
        self.land = land            # Type of land the unit can go onto
        self.position = (0, 0)      # Position of unit on the map
        self.onboard = False        # Indicates if unit is on to a city or another unit
        self.damages = damages      # How much damages this unit does

    def move(self, direction):
        """
        This method moves a unit. Direction is a value between 0 (north) and 7 (north west) going in
        clockwise direction.
        """
        if self.moves<self.movements:
            self.moves += 1
            if direction==0:
                self.position = (self.position[0], self.position[1]-1)
            elif direction==1:
                self.position = (self.position[0]+1, self.position[1]-1)
            elif direction==2:
                self.position = (self.position[0]+1, self.position[1])
            elif direction==3:
                self.position = (self.position[0]+1, self.position[1]+1)
            elif direction==4:
                self.position = (self.position[0], self.position[1]+1)
            elif direction==5:
                self.position = (self.position[0]-1, self.position[1]+1)
            elif direction==6:
                self.position = (self.position[0]-1, self.position[1])
            elif direction==7:
                self.position = (self.position[0]-1, self.position[1]-1)

    def take_damages(self, damages):
        self.hits += damages

    def end_turn(self):
        """
        Reset datas at the end of the turn.
        """
        self.moves = 0
        # If unit is in a city or in another unit, it can repair
        if self.onboard:
            self.hits = 0


class Army(Unit):
    def __init__(self, hits, movements, land):
        super().__init__(hits=1, movements=1, land=1)


class Fighter(Unit):
    def __init__(self, hits, movements, land):
        super().__init__(hits=1, movements=4, land=3)
        self.fuel = 20

    def end_turn(self):
        super().end_turn()
        if self.onboard:
            self.fuel = 20
        else:
            self.fuel -= 1
        # Without fuel, Fighter crashes
        if self.fuel==0
            self.hits = self.max_hits


class Ship(Unit):
    def __init__(self, hits, damages=1):
        super().__init__(hits=hits, movements=2, land=2, damages=damages)

    def take_damages(self, damages):
        super().take_damages(damages)
        # When too much damaged, ships are slower
        if self.hits >= (self.max_hits/2):
            self.movements = 1

    def end_turn(self):
        super().end_turn()
        # If ship is in a city, it can repair
        if self.onboard:
            self.hits = 0


class Destroyer(Ship):
    def __init__(self):
        super().__init__(hits=3)


class TroopTransport(Ship):
    def __init__(self):
        super().__init__(hits=3)
        self.max_carry = 6
        self.carry = []


class Submarine(Ship):
    def __init__(self):
        super().__init__(hits=2, damages=3)


class Cruiser(Ship):
    def __init__(self):
        super().__init__(hits=8)


class AircraftCarrier(Ship):
    def __init__(self):
        super().__init__(hits=8)
        self.max_carry = 8
        self.carry = []


class Battleship(Ship):
    def __init__(self):
        super().__init__(hits=12)


class City():
    def __init__(self):
        self.PRODUCTS = [
            {"Army":                (6,5)},
            {"Fighter":             (12,10)},
            {"Troop transport":     (24,20)},
            {"Destroyer":           (36,30)},
            {"Submarine":           (30,25)},
            {"Cruiser":             (60,50)},
            {"Aircraft carrier":    (72,60)},
            {"Battleship":          (90,75)},
        ]
        self.production = 0
        self.product = None

    def set_production(self, what):
        # New production, old one is lost
        self.production = 0
        self.product = self.PRODUCTS[what][0]

    def end_turn(self):
        if self.product is not None:
            self.production += 1
            if self.production >= self.product:
                #TODO: Create new unit !
                # Since the city will producing the same product, production is faster
                self.product = self.PRODUCTS[what][1]


class Map():
    #TODO: Load a file
    #TODO 2: Generate a map
    pass


class Player():
    def __init__(self):
        # Player's units
        self.units = []

    def end_turn(self):
        # Ending turn of each units
        for unit in self.units:
            unit.end_turn()
            if unit.hits==unit.max_hits:
                # TODO: destroy unit
                pass


class Game():
    def __init__(self, number_of_players):
        # Init a new game
        self.map = Map()
        self.players = []
        for index in range(number_of_player):
            self.players.append(Player(index))
        # Select first player randomly, then go on one by one
        self.actual_player = random.choice(self.players)
        self.turn = 1


