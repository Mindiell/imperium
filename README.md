# Imperium

Imperium is an ASCII based wargame similar to roguelikes. It is based on Walter Bright's Classic
Empire game.

Rules can be found here : http://www.classicempire.com/help.html
 

## Installation

    pip install -r requirements.txt

## Usage

    python imperium.py


