#encoding: utf-8

from plagues import Application
from panels import IntroPanel, AboutPanel, LoginPanel, PasswordPanel

__version__ = "0.1"

class Imperium(Application):
    def __init__(self):
        super().__init__(
            (IntroPanel, AboutPanel, LoginPanel, PasswordPanel),
            "templates"
        )
        self.user = None
        self.login = ""
        self.message = ""
        self.init_log("imperium.log", "DEBUG")
        self.logger.info("Imperium v%s" % __version__)

    def connect(self, password=""):
        """
        This method tries to log in a user
        """
        self.logger.info("Login...")
        self.logger.debug("self.login : %s" % self.login)
        self.logger.debug("password   : %s" % password)
        if self.login=="toto" and password=="titi":
            self.logger.info("Logged!")
            #TODO: when connecting a user, should load its game
            self.user = self.login


if __name__=='__main__':
    Imperium().run()
