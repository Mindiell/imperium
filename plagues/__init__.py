#encoding: utf-8
"""
Plagues is a python framework library for curses.

It aims to give people a better tool to work with curses, not being stopped by placing characters
on screen by lines or calculating if you are writing on another character somewhere.
"""

__version__ = '0.1.5'

from .plagues import Application, Panel, Window, ATTRIBUTES, COLORS
