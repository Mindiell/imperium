#encoding: utf-8

from plagues import Panel

class PasswordPanel(Panel):
    def _init(self):
        self.full_screen("password.tpl")
        self.context = {
            "password": "",
            "login": ""
        }
        self.actions = {
            "\n": self.connect,
            None: self.type_key,
        }

    def before_drawing(self):
        self.context["login"] = self.app.login

    def type_key(self, key):
        self.context["password"] += key

    def connect(self, key):
        self.app.logger.debug("app.login : %s" % self.app.login)
        self.app.logger.debug("password  : %s" % self.context["password"])
        self.app.connect(self.context["password"])
        self.context["password"] = ""
        if self.app.user is not None:
            pass
        self.app.message = "Bad login or password"
        self.app.set_panel("IntroPanel")

