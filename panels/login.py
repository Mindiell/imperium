#encoding: utf-8

from plagues import Panel

class LoginPanel(Panel):
    def _init(self):
        self.full_screen("login.tpl")
        self.context = {
            "user": self.app.user,
            "login": "",
        }
        self.actions = {
            "\n": self.set_login,
            None: self.type_key,
        }

    def type_key(self, key):
        self.context["login"] += key

    def set_login(self, key):
        if self.context["login"]!="":
            self.app.logger.debug("login     : %s" % self.context["login"])
            self.app.login = self.context["login"]
            self.context["login"] = ""
            self.app.logger.debug("app.login : %s" % self.app.login)
            self.app.set_panel("PasswordPanel")
        else:
            self.app.set_panel("IntroPanel")
