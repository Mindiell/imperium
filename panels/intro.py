#encoding: utf-8

from plagues import Panel

class IntroPanel(Panel):
    def __init__(self, app):
        super().__init__(app)
        self.full_screen("intro.tpl")
        self.actions = {
            # Quit application
            "q": lambda key: True,
            # Display about panel
            "a": lambda key: self.app.set_panel("AboutPanel"),
            # Login
            "l": lambda key: self.app.set_panel("LoginPanel"),
            # Disconnect logged user
            "d": self.disconnect,
        }

    def before_drawing(self):
        self.context["user"] = self.app.user

    def disconnect(self, key):
        self.app.user = None
