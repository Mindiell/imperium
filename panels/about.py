#encoding: utf-8

from plagues import Panel

class AboutPanel(Panel):
    def _init(self):
        self.full_screen("about.tpl")
        self.actions = {
            # Get back to Intro panel
            "q": lambda key: self.app.set_panel("IntroPanel"),
        }
